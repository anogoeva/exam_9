import React from 'react';
import {useHistory} from "react-router-dom";
import './NavigationItems.css';

const NavigationItems = () => {
    const history = useHistory();
    const goFormPage = async e => {
        history.push('/Form')
    };
    return (
        <button className="ContactAdd" onClick={goFormPage}>Add new contact</button>
    );
};

export default NavigationItems;