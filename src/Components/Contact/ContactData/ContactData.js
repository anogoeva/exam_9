import React from 'react';
import {useHistory} from "react-router-dom";
import {useDispatch} from "react-redux";
import {deleteContact} from "../../../store/actions/contactsActions";

const ContactData = (props) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const goEditPage = async e => {
        history.push('/edit/' + props.contact.key)
    };
    const deleteContactId = (e) => {
        e.preventDefault();
        dispatch(deleteContact(props.contact.key, false));
        history.push('/');
    };

    return (
        <div>
            <img src={props.contact.photoUrl} alt="contact" width="60" height="60" className="itemImg"/>
            <p>{props.contact.name}</p>
            <p>{props.contact.phone}</p>
            <p>{props.contact.email}</p>
            <button onClick={goEditPage}>Edit</button>
            <button onClick={deleteContactId}>Delete</button>
        </div>
    );
};

export default ContactData;