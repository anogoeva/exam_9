import React from 'react';
import './Contact.css';

const Contact = (props) => {

    return (
        <div className="contactItem">
            <div><img className="item itemImg" src={props.image} alt="contact" width="50" height="45"/></div>
            <div><h3 className="item">{props.name}</h3></div>
        </div>
    );
};

export default Contact;