import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from "react-router-dom";
import {createContacts, editContact} from "../../store/actions/contactsActions";
import Spinner from "../UI/Spinner/Spinner";

const FormPage = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const contact = useSelector(state => state.contactReducers.contact);
    const loading = useSelector(state => state.contactReducers.loading);

    const [inputBody, setInputBody] = useState({
        name: '',
        phone: '',
        email: '',
        photoUrl: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setInputBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const addContact = (e) => {
        if (history.location.pathname.includes('edit')) {
            e.preventDefault();
            const id = history.location.pathname.slice(6);
            dispatch(editContact(id, obj, false));
            history.push('/')
        } else {
            inputBody.photoUrl = photoUrlCheck;
            e.preventDefault();
            dispatch(createContacts(inputBody));
            history.push('/');
        }
    };

    let photoUrlCheck = null;
    if (contact.photoUrl) {
        photoUrlCheck = contact.photoUrl
    } else if (inputBody.photoUrl) {
        photoUrlCheck = inputBody.photoUrl;
    } else if (inputBody.photoUrl === ''){
        photoUrlCheck = 'https://previews.123rf.com/images/eveleen/eveleen1712/eveleen171200013/91725292-anonymous-person-icon-.jpg'
    }

    const name = contact.name ? contact.name : inputBody.name;
    const phone = contact.phone ? contact.phone : inputBody.phone;
    const photoUrl = contact.photoUrl ? contact.photoUrl : inputBody.photoUrl;
    const email = contact.email ? contact.email : inputBody.email;
    const obj = {
        name: inputBody.name ? inputBody.name : name,
        phone: inputBody.phone ? inputBody.phone : phone,
        photoUrl: inputBody.photoUrl ? inputBody.photoUrl : photoUrl,
        email: inputBody.email ? inputBody.email : email
    };

    let div = null;

    if (loading) {
        div = <Spinner/>
    }

    return (
        <>
            {div}
        <form onSubmit={addContact}>
            <div className="EditAddForm">
                <h4>Title</h4>
                <h5>Name:</h5>
                <input className="form"
                       name="name"
                       type="text"
                       defaultValue={name}
                       onChange={onInputChange}/>
                <p>Phone:</p>
                <input className="form"
                       name="phone"
                       type="number"
                       defaultValue={phone}
                       onChange={onInputChange}/>
                <p>Email:</p>
                <input className="form"
                       name="email"
                       type="email"
                       defaultValue={email}
                       onChange={onInputChange}/>
                <p>Photo:</p>
                <p><input className="form"
                          name="photoUrl"
                          type="text"
                          defaultValue={
                              photoUrlCheck
                          }
                          onChange={onInputChange}/></p>
                <img src={photoUrlCheck} className="itemImg"
                     alt="preview"
                     width="60"
                     height="60"
                />
            </div>
            <button type="submit" className="btn">Send</button>
        </form>
        </>
    );
};

export default FormPage;