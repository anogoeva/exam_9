import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./Components/UI/Layout/Layout";
import FormPage from "./Components/FormPage/FormPage";
import ContactPage from "./container/ContactPage/ContactPage";
import './App.css';

const App = () => (
    <BrowserRouter>
        <Layout>
            <Switch>
                <Route path="/" exact component={ContactPage}/>
                <Route path="/Form" component={FormPage}/>
                <Route path="/edit/:id" component={FormPage}/>
            </Switch>
        </Layout>
    </BrowserRouter>
);

export default App;
