import axiosApi from "../../axiosApi";

export const ADD_CONTACT_REQUEST = 'ADD_CONTACT_REQUEST';
export const ADD_CONTACT_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const ADD_CONTACT_FAILURE = 'ADD_CONTACT_FAILURE';

export const addContactRequest = () => ({type: ADD_CONTACT_REQUEST});
export const addContactSuccess = contactsData => ({type: ADD_CONTACT_SUCCESS, payload: contactsData});
export const addContactFailure = error => ({type: ADD_CONTACT_FAILURE, payload: error});

export const EDIT_CONTACT_REQUEST = 'EDIT_CONTACT_REQUEST';
export const EDIT_CONTACT_SUCCESS = 'EDIT_CONTACT_SUCCESS';
export const EDIT_CONTACT_FAILURE = 'EDIT_CONTACT_FAILURE';

export const editContactRequest = () => ({type: EDIT_CONTACT_REQUEST});
export const editContactSuccess = (id, contactData, closeModal) => ({
    type: EDIT_CONTACT_SUCCESS,
    payload: contactData,
    id: id,
    closeModal: closeModal
});
export const editContactFailure = error => ({type: EDIT_CONTACT_FAILURE, payload: error});

export const DELETE_CONTACT_REQUEST = 'DELETE_CONTACT_REQUEST';
export const DELETE_CONTACT_SUCCESS = 'DELETE_CONTACT_SUCCESS';
export const DELETE_CONTACT_FAILURE = 'DELETE_CONTACT_FAILURE';

export const deleteContactRequest = () => ({type: DELETE_CONTACT_REQUEST});
export const deleteContactSuccess = (contactsData, showModal) => ({
    type: DELETE_CONTACT_SUCCESS,
    payload: contactsData,
    showModal: showModal
});
export const deleteContactFailure = error => ({type: DELETE_CONTACT_FAILURE, payload: error});

export const FETCH_CONTACTS_REQUEST = 'ADD_CONTACT_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'ADD_CONTACT_SUCCESS';
export const FETCH_CONTACTS_FAILURE = 'ADD_CONTACT_FAILURE';

export const SET_MODAL_OPEN = 'SET_MODAL_OPEN';

export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = contactsData => ({type: FETCH_CONTACTS_SUCCESS, payload: contactsData});
export const fetchContactsFailure = error => ({type: FETCH_CONTACTS_FAILURE, payload: error});

export const setModalOpen = (isOpen, key, name, phone, photoUrl, email) => ({
    type: SET_MODAL_OPEN,
    payload: isOpen,
    key: key,
    name: name,
    phone: phone,
    photoUrl: photoUrl,
    email: email
});

export const createContacts = contactsData => {
    return async dispatch => {
        try {
            dispatch(addContactRequest());
            const response = await axiosApi.post('/contacts.json', contactsData);
            if (response.data) {
                const obj = {};
                obj[response.data.name] = contactsData;
                dispatch(addContactSuccess(obj));
            }
        } catch (e) {
            dispatch(addContactFailure())
        }
    }
};

export const fetchContacts = () => {
    return async dispatch => {
        try {
            dispatch(fetchContactsRequest());
            const response = await axiosApi.get('/contacts.json');
            dispatch(fetchContactsSuccess(response.data));
        } catch (e) {
            dispatch(fetchContactsFailure())
        }
    }
};

export const deleteContact = (id, showModal) => {
    return async dispatch => {
        try {
            dispatch(deleteContactRequest());
            const response = await axiosApi.delete('/contacts/' + id + '.json');
            if (response.data === null) {
                dispatch(deleteContactSuccess(id, showModal));
            }
        } catch (e) {
            dispatch(deleteContactFailure())
        }
    }
};

export const editContact = (id, contactData, closeModal) => {
    return async dispatch => {
        try {
            dispatch(editContactRequest());
            const response = await axiosApi.put('/contacts/' + id + '.json', contactData);
            dispatch(editContactSuccess(id, response.data, closeModal));
        } catch (e) {
            dispatch(editContactFailure())
        }
    }
};


