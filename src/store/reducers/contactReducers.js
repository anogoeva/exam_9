import {
    ADD_CONTACT_FAILURE,
    ADD_CONTACT_REQUEST,
    ADD_CONTACT_SUCCESS,
    DELETE_CONTACT_FAILURE,
    DELETE_CONTACT_REQUEST,
    DELETE_CONTACT_SUCCESS,
    EDIT_CONTACT_FAILURE,
    EDIT_CONTACT_REQUEST,
    EDIT_CONTACT_SUCCESS,
    FETCH_CONTACTS_FAILURE,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    SET_MODAL_OPEN
} from "../actions/contactsActions";

const initialState = {
    contact: {},
    contacts: {},
    loading: false,
    error: null,
    showPurchaseModal: false
};

const contactReducers = (state = initialState, action) => {
    switch (action.type) {
        case ADD_CONTACT_REQUEST:
            return {...state, loading: true};
        case ADD_CONTACT_SUCCESS:
            return {
                ...state, loading: false,
                contacts: {
                    ...state.contacts,
                    ...action.payload
                }
            };
        case ADD_CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload};
        case FETCH_CONTACTS_REQUEST:
            return {...state, loading: true};
        case FETCH_CONTACTS_SUCCESS:
            return {
                ...state, loading: false,
                contacts: {
                    ...state.contacts,
                    ...action.payload
                }
            };
        case FETCH_CONTACTS_FAILURE:
            return {...state, loading: false, error: action.payload};
        case SET_MODAL_OPEN:
            return {
                ...state, showPurchaseModal: action.payload,
                contact: {
                    ...state.contact,
                    key: action.key,
                    name: action.name,
                    phone: action.phone,
                    photoUrl: action.photoUrl,
                    email: action.email
                }
            };
        case DELETE_CONTACT_REQUEST:
            return {...state, loading: true};
        case DELETE_CONTACT_SUCCESS:
            const contacts = {...state.contacts};
            delete contacts[action.payload];
            return {...state, loading: false, showPurchaseModal: action.showModal, contacts: contacts};
        case DELETE_CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload};
        case EDIT_CONTACT_REQUEST:
            return {...state, loading: true};
        case EDIT_CONTACT_SUCCESS:
            const editContact = {...state.contacts};
            editContact[action.id] = action.payload;
            return {
                ...state, loading: false, ...state.contacts, contacts: editContact, showPurchaseModal: action.closeModal
            };
        case EDIT_CONTACT_FAILURE:
            return {...state, loading: false, error: action.payload};
        default:
            return state;
    }
};
export default contactReducers;
