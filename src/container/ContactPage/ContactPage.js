import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Contact from "../../Components/Contact/Contact";
import './ContactPage.css';
import Modal from "../../Components/UI/Modal/Modal";
import ContactData from "../../Components/Contact/ContactData/ContactData";
import Spinner from "../../Components/UI/Spinner/Spinner";
import {fetchContacts, setModalOpen} from "../../store/actions/contactsActions";

const ContactPage = () => {
    const contacts = useSelector(state => state.contactReducers.contacts);
    const contact = useSelector(state => state.contactReducers.contact);
    const loading = useSelector(state => state.contactReducers.loading);
    const showPurchaseModal = useSelector(state => state.contactReducers.showPurchaseModal);
    const purchaseCancelHandler = () => {
        dispatch(setModalOpen(false));
    };
    const purchaseHandler = (key, name, phone, photoUrl, email) => {
        dispatch(setModalOpen(true, key, name, phone, photoUrl, email));
    };
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchContacts());
    }, [dispatch]);

    let div = null;

    if (loading) {
        div = <Spinner/>
    }

    return (
        <div>
            {div}
            <Modal
                show={showPurchaseModal}
                close={purchaseCancelHandler}>
                <ContactData contact={contact}/>
            </Modal>
            {contacts && Object.entries(contacts).map(([key, value], i) => {
                return (
                    <div className="contact"
                         onClick={() => purchaseHandler(key, value.name, value.phone, value.photoUrl, value.email)}
                         key={key}>
                        <Contact
                            name={value.name}
                            image={value.photoUrl}
                        />
                    </div>
                );
            })}
        </div>
    );
};

export default ContactPage;